#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
float val1=0, val2=0;
int var=0;
puts("Enter two variables");
scanf("%f %f", &val1, &val2);
printf("What do you want?\n1. Add\n2. Subtract\n3. Multiply\n4. Divide\n");
scanf("%d", &var);
switch (var)
	{
	case 1:
		printf("Result = %f\n", val1+val2);
		break;
	case 2:
		printf("Result = %f\n", val1-val2);
		break;
	case 3:
		printf("Result = %f\n", val1*val2);
		break;
	case 4:
		printf("Result = %f\n", val1/val2);
		break;
	default:
		printf("Input Error!\n");
	}
puts("Thank you for using this program!");
return 0;
}
